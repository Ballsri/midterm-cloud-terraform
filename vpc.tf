resource "aws_vpc" "main" {
  cidr_block = "172.16.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "midterm-cloud-vpc"
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main.id
}

resource "aws_subnet" "public_app" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "172.16.1.0/24"
  availability_zone =  var.availability_zone
  map_public_ip_on_launch = true
}

resource "aws_subnet" "public_nat" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "172.16.2.0/24"
  availability_zone =  var.availability_zone
}

resource "aws_subnet" "private_app_db" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "172.16.3.0/24"
  availability_zone =  var.availability_zone  
}

resource "aws_subnet" "private_db" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "172.16.4.0/24"
  availability_zone =  var.availability_zone
}


resource "aws_nat_gateway" "nat" {
  allocation_id = aws_eip.nat.id
  subnet_id     = aws_subnet.public_nat.id
}

resource "aws_network_interface" "app_public" {
  subnet_id       = aws_subnet.public_app.id
  security_groups = [aws_security_group.allow_web.id, aws_security_group.allow_ssh.id]
}

resource "aws_network_interface" "app_db_private" {
  subnet_id       = aws_subnet.private_app_db.id
  private_ip = "172.16.3.9"
  depends_on = [aws_instance.app]
}

resource "aws_network_interface" "db_app_private" {
  subnet_id       = aws_subnet.private_app_db.id
  private_ip = "172.16.3.10"
  depends_on = [aws_instance.db]
}

resource "aws_network_interface" "db_private" {
  subnet_id       = aws_subnet.private_db.id
  private_ip = "172.16.4.10"
  security_groups = [aws_security_group.allow_ssh.id, aws_security_group.allow_db.id]
}

resource "aws_eip" "nat" {
}

resource "aws_eip" "app_eip" {
}

resource "aws_ec2_instance_connect_endpoint" "db_connect_endpoint" {
  subnet_id = aws_subnet.private_db.id
  security_group_ids = [aws_security_group.allow_ssh.id]
  
}