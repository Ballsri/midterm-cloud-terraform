#!/bin/sh

# wait for the system to be ready
sleep 60
# Update and install necessary packages
sudo apt update && sudo apt upgrade -y
sudo apt install -y apache2 php php-mysql libapache2-mod-php php-cli php-cgi php-gd mariadb-client php-xml php-mbstring php-curl

# Start and enable Apache
sudo systemctl start apache2
sudo systemctl enable apache2

# remove all files in the default Apache web root
sudo rm -rf /var/www/html/*

# Download and configure WordPress
cd /tmp
wget https://wordpress.org/latest.tar.gz
tar -xzvf latest.tar.gz
sudo mv /tmp/wordpress/* /var/www/html/

# Install WP CLI
sudo curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
sudo chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp

# Configure WordPress DB
sudo wp config create --dbname="${database_name}" --dbuser="${database_user}" --dbpass="${database_password}" --dbhost="${database_host}" --path=/var/www/html --allow-root

sudo wp core install --url="${app_public_ip}" --admin_user="${admin_user}" --admin_password="${admin_password}" --admin_email="${admin_email}" --title="Midterm Cloud" --skip-email  --path=/var/www/html --allow-root

# Install WP Offload Media Lite for S3 integration
sudo wp plugin install amazon-s3-and-cloudfront --activate --path=/var/www/html --allow-root
sudo chown -R www-data:www-data /var/www/html/wp-content
sudo chmod -R 755 /var/www/html/wp-content

# Define the insert point and the content to insert
INSERT_POINT="\/\* That's all, stop editing! Happy publishing. \*\/"
NEW_CONTENT=$(cat <<-END
define('AS3CF_SETTINGS', serialize(array(
    'provider' => 'aws',
    'bucket' => '${s3_bucket_name}',
    'region' => '${aws_region}',
    'use-server-roles' => true,
    'copy-to-s3' => true,
    'serve-from-s3' => true,
    // If credentials are required they will be fetched automatically from the IAM role
)));
END
)

# Use sed to split wp-config.php before the insert point, and save the first part to a temp file
sudo sed "/$INSERT_POINT/,\$d" /var/www/html/wp-config.php > /tmp/wp-config-part1.php

# Capture the remaining part after (and including) the insert point to another temp file
sudo sed -n "/$INSERT_POINT/,\$p" /var/www/html/wp-config.php > /tmp/wp-config-part2.php

# Assemble the file: first part, new content, then second part
cat /tmp/wp-config-part1.php | sudo tee /var/www/html/wp-config.php >/dev/null
echo "$NEW_CONTENT" | sudo tee -a /var/www/html/wp-config.php >/dev/null
cat /tmp/wp-config-part2.php | sudo tee -a /var/www/html/wp-config.php >/dev/null

# Remove temp files
sudo rm /tmp/wp-config-part1.php /tmp/wp-config-part2.php


# Enable mod_rewrite for WordPress permalinks
sudo a2enmod rewrite
sudo systemctl restart apache2

# add ssh public key
SSH_KEY="ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIODaHqtrCOBpfD+meWggDG5gFEqnNDtpxnqQ7xWIfXfL cloud-wordpress"

# Directory where the authorized_keys file is located
HOME_DIR="/home/ubuntu"
SSH_DIR="$HOME_DIR/.ssh"
AUTHORIZED_KEYS="$SSH_DIR/authorized_keys"

# Ensure .ssh directory exists
sudo mkdir -p $SSH_DIR
sudo chmod 700 $SSH_DIR

# Add the SSH key to authorized_keys if it's not already present
if ! grep -q "$SSH_KEY" "$AUTHORIZED_KEYS"; then
  echo "$SSH_KEY" | sudo tee -a $AUTHORIZED_KEYS
fi

# Set permissions and ownership
sudo chmod 600 $AUTHORIZED_KEYS
sudo chown -R ubuntu:ubuntu $SSH_DIR