resource "aws_instance" "app" {
    ami           = var.ami
    instance_type = "t2.micro"
    iam_instance_profile = aws_iam_instance_profile.app_instance_profile.name

    network_interface {
        device_index = 0
        network_interface_id = aws_network_interface.app_public.id
    }
    user_data = data.template_file.app_init_script.rendered
    tags = {
        Name = "midterm-cloud-app"
    }
    depends_on = [ aws_eip.app_eip, aws_instance.db]
}

resource "aws_network_interface_attachment" "app_db_private_attachment" {
  instance_id          = aws_instance.app.id
  network_interface_id = aws_network_interface.app_db_private.id
  device_index         = 1
  depends_on = [aws_instance.app, aws_network_interface.app_db_private]
}

resource "aws_eip_association" "app_eip_assoc" {
  network_interface_id = aws_network_interface.app_public.id
  allocation_id = aws_eip.app_eip.id
  depends_on = [ aws_instance.app, aws_eip.app_eip, aws_network_interface.app_public ]
}



resource "aws_instance" "db" {
    ami           = var.ami
    instance_type = "t2.micro"
    user_data = data.template_file.db_init_script.rendered

    network_interface {
        device_index = 0
        network_interface_id = aws_network_interface.db_private.id
    }

    tags = {
        Name = "midterm-cloud-db"
    }
}

resource "aws_network_interface_attachment" "db_app_attachment" {
    device_index = 1
    instance_id          = aws_instance.db.id
    network_interface_id = aws_network_interface.db_app_private.id
    depends_on = [aws_instance.db, aws_network_interface.db_app_private]
}



