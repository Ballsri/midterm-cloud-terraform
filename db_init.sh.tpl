#!/bin/sh
# wait for the system to be ready
sleep 60

sudo apt update -y

sudo apt install -y mariadb-server

sudo systemctl start mariadb

sudo systemctl enable mariadb


sudo mysql -u root <<_EOF_
CREATE DATABASE ${database_name};
CREATE USER '${database_user}'@'%' IDENTIFIED BY '${database_password}';

GRANT ALL PRIVILEGES ON ${database_name}.* TO '${database_user}'@'%';

# Apply new permissions
FLUSH PRIVILEGES;

# Exit the MySQL shell
EXIT;
_EOF_

# concat [mysqld] to the end of the file
sudo tee -a /etc/mysql/my.cnf <<EOF
[mysqld]
bind-address = 0.0.0.0
EOF

sudo systemctl restart mariadb

# add ssh public key
SSH_KEY="ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIODaHqtrCOBpfD+meWggDG5gFEqnNDtpxnqQ7xWIfXfL cloud-wordpress"

# Directory where the authorized_keys file is located
HOME_DIR="/home/ubuntu"
SSH_DIR="$HOME_DIR/.ssh"
AUTHORIZED_KEYS="$SSH_DIR/authorized_keys"

# Ensure .ssh directory exists
sudo mkdir -p $SSH_DIR
sudo chmod 700 $SSH_DIR

# Add the SSH key to authorized_keys if it's not already present
if ! grep -q "$SSH_KEY" "$AUTHORIZED_KEYS"; then
  echo "$SSH_KEY" | sudo tee -a $AUTHORIZED_KEYS
fi

# Set permissions and ownership
sudo chmod 600 $AUTHORIZED_KEYS
sudo chown -R ubuntu:ubuntu $SSH_DIR