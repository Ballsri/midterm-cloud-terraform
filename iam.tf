resource "aws_iam_role" "s3_access_role" {
  name = "midterm_cloud_app_s3_access_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "ec2.amazonaws.com"
        },
      },
    ]
  })
}

resource "aws_iam_policy" "s3_bucket_access" {
  name        = "midterm_cloud_app_s3_bucket_access"
  description = "Policy that grants access to a specific S3 bucket."

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = [
            "s3:*",
        ],
        Effect = "Allow",
        Resource = [
          "${aws_s3_bucket.my_bucket.arn}",
          "${aws_s3_bucket.my_bucket.arn}/*"
        ],
      },
    ]
  })
}

resource "aws_iam_role_policy_attachment" "s3_access_attachment" {
  role       = aws_iam_role.s3_access_role.name
  policy_arn = aws_iam_policy.s3_bucket_access.arn
}

resource "aws_iam_instance_profile" "app_instance_profile" {
  role = aws_iam_role.s3_access_role.name
}
