variable "region" {}
variable "availability_zone" {}
variable "ami" {}
variable "bucket_name" {}
variable "database_name" {}
variable "database_user" {}
variable "database_pass" {}
variable "admin_user" {}
variable "admin_pass" {}

data "template_file" "app_init_script" {
  template = file("${path.module}/app_init.sh.tpl")

  vars = {
    s3_bucket_name = var.bucket_name
    database_name = var.database_name
    database_user = var.database_user
    database_password = var.database_pass
    database_host = aws_network_interface.db_private.private_ip
    aws_region = var.region
    app_public_ip = aws_eip.app_eip.public_ip
    admin_user = var.admin_user
    admin_password = var.admin_pass
    admin_email = "example@example.com"
  }
}

data "template_file" "db_init_script" {
  template = file("${path.module}/db_init.sh.tpl")

    vars = {
        database_name = var.database_name
        database_user = var.database_user
        database_password = var.database_pass
    }
}